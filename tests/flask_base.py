import pytest
from app import create_app
from bs4 import BeautifulSoup
# import chromedriver_binary  # Adds chromedriver binary to path
# from selenium import webdriver

# @pytest.fixture()
# def app():
#     app = create_app(True)
#     yield app

# @pytest.fixture()
# def client(app):
#     return app.test_client()

# @pytest.fixture()
# def runner(app):
#     return app.test_cli_runner()

# driver = webdriver.Chrome()


app = create_app(True)

client = app.test_client()

runner = app.test_cli_runner()


def get_html(response):
    html_text = response.get_data(as_text=True)
    soup = BeautifulSoup(html_text, 'html.parser')
    return soup